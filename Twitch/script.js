const express = require('express');
const app = express();
const axios = require('axios');
let prompt = require('prompt');
const Json2csvParser = require('json2csv').Parser;
let fs = require('fs');
let sleep = require('sleep');


const client_id = "tc05qfu8b4au0971m8rsb0gthpxhsa";
const secret = "8bcz1o1r4ppzx0n4s2matk572k8axz";
let access_token = null;
let follower_data = null;
let channel = null;

function login() {
    axios.post('https://id.twitch.tv/oauth2/token?client_id=' + client_id + '&client_secret=' + secret + '&grant_type=client_credentials' +
        '&scope=user_subscriptions+user_read')
        .then(response => {
            access_token = response.data.access_token;
        })
        .catch(error => {
            console.log(error);
        });
}

function getChannel() {
    login();
    prompt.start();
    prompt.get(['channel'], function (err, result) {
        console.log('Input received:');
        console.log('Channel:' + result.channel);
        getFollowers(result.channel);
    });
}

function getFollowers(channel) {
    axios.get('https://api.twitch.tv/kraken/channels/'+channel+'/follows?limit=2&offset=0&client_id=' + client_id)
        .then(response => {
            let total = response.data._total;
            console.log(total);
            let loops_required = Math.floor(total/100);
            let offset = 0;

            for(  let i = 0; i < loops_required; i++)
            {
                if(offset <= 1600) {
                    axios.get('https://api.twitch.tv/kraken/channels/' + channel + '/follows?limit=100&offset=' + offset + '&client_id=' + client_id)
                        .then(response => {
                            follower_data = response.data;
                            for (let follower in follower_data) {
                                let f = follower_data[follower];
                                saveInFile(channel, f, i);
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }
                offset = offset+100;
            }

        })
        .catch(error => {
            console.log(error);
        });
}

function saveInFile(channel,followers,i){
    let userArray = [];

    for(let follower in followers){
        if(followers[follower].user) {
            let user = followers[follower].user;
            try {

                let json_object = {
                    "Channel": channel,
                    "Display Name": user.display_name,
                    "ID":user._id,
                    "Username":user.name,
                    "Type": user.type,
                    "Bio": user.bio,
                    "Link": 'https://twitch.tv/'+user.name
                };
                userArray.push(json_object);

            } catch (err) {
                console.error(err);
            }
        }
    }
    if(userArray.length)
    {
         const fields = ['Channel', 'Display Name', 'ID', 'Username', 'Type', 'Bio', 'Link'];
         const json2csvParser = new Json2csvParser({ fields });
         const data = json2csvParser.parse(userArray);
        fs.writeFile("output/test_"+i+".csv",data, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }

}

getChannel();

