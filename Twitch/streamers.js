const express = require('express');
const app = express();
const axios = require('axios');
let prompt = require('prompt');
const Json2csvParser = require('json2csv').Parser;
let fs = require('fs');
let sleep = require('sleep');


const client_id = "tc05qfu8b4au0971m8rsb0gthpxhsa";
const secret = "8bcz1o1r4ppzx0n4s2matk572k8axz";
let access_token = null;

function login() {
    axios.post('https://id.twitch.tv/oauth2/token?client_id=' + client_id + '&client_secret=' + secret + '&grant_type=client_credentials' +
        '&scope=user_read')
        .then(response => {
            access_token = response.data.access_token;
        })
        .catch(error => {
            console.log(error);
        });
}

function getStreamers() {
    login();
    axios.get('https://api.twitch.tv/helix/streams', {headers: {'Client-ID': client_id}
    }).then(response => {
            let stream_data = response.data.data;
            for (let stream in stream_data) {
                if(stream_data[stream].user_id)
                {
                    let user_id = stream_data[stream].user_id;
                    getUserDetails(user_id);
                }
           }
        })
        .catch(error => {
            console.log(error);
        });
}

function getUserDetails(user_id) {
    axios.get('https://api.twitch.tv/kraken/users/'+user_id, {headers: {'Client-ID': client_id, 'Accept': 'application/vnd.twitchtv.v5+json'}
    }).then(response => {
        console.log(response.data);
    })
        .catch(error => {
            console.log(error);
        });
}

function saveInFile(channel,followers,i){
    let userArray = [];

    for(let follower in followers){
        if(followers[follower].user) {
            let user = followers[follower].user;
            try {

                let json_object = {
                    "Channel": channel,
                    "Display Name": user.display_name,
                    "ID":user._id,
                    "Username":user.name,
                    "Type": user.type,
                    "Bio": user.bio,
                    "Link": 'https://twitch.tv/'+user.name
                };
                userArray.push(json_object);

            } catch (err) {
                console.error(err);
            }
        }
    }
    if(userArray.length)
    {
        const fields = ['Channel', 'Display Name', 'ID', 'Username', 'Type', 'Bio', 'Link'];
        const json2csvParser = new Json2csvParser({ fields });
        const data = json2csvParser.parse(userArray);
        fs.writeFile("output/test_"+i+".csv",data, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }

}

getStreamers();

