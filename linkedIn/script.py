from bs4 import BeautifulSoup
from selenium import webdriver
import time
import csv

c = csv.writer(open("linkedin-group-results.csv", "w"))
c.writerow(["Member", "Profile"])
driver = webdriver.Chrome(executable_path=r'/home/panther/projects/liveedu/linkedIn/chromedriver')

your_groups_code = """

<div tabindex="-1" class="focus-first"><h2 class="a11y-text" tabindex="0">The list of members</h2><ul 
class="manage-members-list"><li class="member-view"><div class="select-block"><input type="checkbox" 
class="js-select-input select-input small-input" id="select-member-5019602-60613"><label 
for="select-member-5019602-60613"><span class="a11y-text">Select member</span></label></div><div 
class="member-block"><div class="member-entity "><a class="entity-container entity-link js-member-entity-link" 
href="https://www.linkedin.com/in/georgeezzat"><figure class="entity-figure"><img 
src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C5103AQF1VB8lwqcl7g/profile-displayphoto
-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=jX1vgUzIQcj1MaE1ZYuO3db3GBKryRMMMg0Yh42ROH4" class="entity-image 
member-image " alt="George Ezzat" width="100" height="100"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAABMl9IBo908Bjy_AfYKVIE8bkPAua-_wBg-60613" id="hovercard-613">George Ezzat</span></p><p 
class="entity-headline">Founder and Board Member of Multiple Companies</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=5019602&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-72424539-60613"><label for="select-member-72424539-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/chinedu-oniah-42330820"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C4D03AQG4e9x0CdZDsQ
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=8eFLT1v-6_vraLDjBosQbmVorZi8I4XZSmtUMouGbNk" 
class="entity-image member-image " alt="Chinedu Stan Oniah" width="100" height="100"></figure><div 
class="entity-info"><p class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAARRHFsBfe2tWHIjA6uyCv_TuJuYPvuBd8o-60613" id="hovercard-614">Chinedu Stan Oniah</span></p><p 
class="entity-headline">LEAD ICT/INFRASTRUCTURE;  CFIP, PRINCE 2, ITIL, COBIT5, MIPMA, PMIIM</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=72424539&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-53223770-60613"><label for="select-member-53223770-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/uttam-khot-9629a815"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C4D03AQFCh0onafw3Yw
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=TccOWeoz-Y922vgBFaHLkL0T2FEpATqnc_5zRaLeW6Q" 
class="entity-image member-image " alt="Uttam Khot" width="100" height="100"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAAMsIVoBYgm30VWIt6u3Mjl0gAqEbH6-GmY-60613" id="hovercard-615">Uttam Khot</span></p><p 
class="entity-headline">Sales Engineer II at Rapid7</p><p class="entity-subheading"></p></div></a></div></div><ul 
class="member-actions-list"><li class="member-action-item"><a class="js-message-member message-member" 
title="Message" href="https://www.linkedin.com/messaging/compose?connId=53223770&amp;groupId=60613"><li-icon 
type="envelope-icon" size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 
100%;"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" 
class="icon"><g class="small-icon" style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,
1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,
0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> </g></svg></svg></li-icon><span 
class="action-text">Message</span></a></li></ul></li><li class="member-view"><div class="select-block"><input 
type="checkbox" class="js-select-input select-input small-input" id="select-member-17139617-60613"><label 
for="select-member-17139617-60613"><span class="a11y-text">Select member</span></label></div><div 
class="member-block"><div class="member-entity "><a class="entity-container entity-link js-member-entity-link" 
href="https://www.linkedin.com/in/sheikmasood"><figure class="entity-figure"><img 
src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C4E03AQHN5kzxW01ggA/profile-displayphoto
-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=2PSHNlQ505lmBXKplPDPDk84GRnZyQs79_-DtFlGz8E" class="entity-image 
member-image " alt="Masood Sheik" width="100" height="100"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAAEFh6EBawz_dHtVNo5vreZnvCYrju-O18c-60613" id="hovercard-616">Masood Sheik</span></p><p 
class="entity-headline">BLOCKCHAIN, Cyber Security &amp; Technology Business Consulting </p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=17139617&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-156144183-60613"><label for="select-member-156144183-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/oladimeji-adepoju-itil-cisa-ccnp-mcse-27336144
"><figure class="entity-figure"><img src="https://static.licdn.com/sc/p/com.linkedin.communities-frontend
%3Acommunities-frontend-static-content%2B0.1.232/f/%2Fcommunities-frontend%2Fimages%2Fghost%2Fprofile-100.png" 
class="entity-image member-image " alt="Oladimeji Adepoju ITIL CISA CCNP MCSE"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAAlOkjcBn3_dl1vx7ndNR1jNHHyayqe3eUM-60613" id="hovercard-617">Oladimeji Adepoju ITIL CISA 
CCNP MCSE</span></p><p class="entity-headline">ITIL V3 | CISA | CCNP | CompTia A+ | MCP  | MCSA | MCSE</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=156144183&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-55644226-60613"><label for="select-member-55644226-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/simphiwemayisela"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C4E03AQF154mBzGhG8Q
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=MKb6bWQRM-IrecYYm60DAoA7OUa9lO0HgJBMCYlWRIU" 
class="entity-image member-image " alt="Simphiwe Mayisela" width="100" height="100"></figure><div 
class="entity-info"><p class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAANREEIBRSMTepXjA3PM7wEsjXSjfwxaXEI-60613" id="hovercard-618">Simphiwe Mayisela</span></p><p 
class="entity-headline">Head: Information Security, Risk and Governance at PIC</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=55644226&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-125482608-60613"><label for="select-member-125482608-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/francis-kakula-30003536"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C5603AQEMx9VMFvEs_Q
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=GbbihpKYmrvNuaQaoDGnoP0CX5KSmAbNH6a1LB-bPwc" 
class="entity-image member-image " alt="Francis Kakula" width="100" height="100"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAAAd6tnAB2wYx5zM0TnF03B7k196gcfFUzDI-60613" id="hovercard-619">Francis Kakula</span></p><p 
class="entity-headline">Business Continuity Manager at Exim Bank (Tanzania) Limited</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=125482608&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-607913165-60613"><label for="select-member-607913165-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/selwyn-hanslo-72570914b"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C5603AQFjz7dJWRL91g
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=749oL4tWVR1Afu9SfjXUfdugpEr6YEDcQxnFt6Fr3KI" 
class="entity-image member-image " alt="Selwyn Hanslo" width="100" height="100"></figure><div class="entity-info"><p 
class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAACQ8BM0B9iea82zTXyrP4XzhF63NvADT8gc-60613" id="hovercard-620">Selwyn Hanslo</span></p><p 
class="entity-headline">Enterprise Architect at NEC South Africa</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=607913165&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-217130796-60613"><label for="select-member-217130796-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/voke-benjamin-a-49087260"><figure 
class="entity-figure"><img src="https://static.licdn.com/sc/p/com.linkedin.communities-frontend%3Acommunities
-frontend-static-content%2B0.1.232/f/%2Fcommunities-frontend%2Fimages%2Fghost%2Fprofile-100.png" class="entity-image 
member-image " alt="Voke Benjamin A."></figure><div class="entity-info"><p class="entity-name"><span 
class="js-hovercard entity-name-text" data-membership-id="ADcAAAzxJywBmMCO8QP3d_9LLJk3sFUwM-p-jGY-60613" 
id="hovercard-621">Voke Benjamin A.</span></p><p class="entity-headline">Sales Professional/Technology 
Consultant</p><p class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li 
class="member-action-item"><a class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=217130796&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li><li class="member-view"><div 
class="select-block"><input type="checkbox" class="js-select-input select-input small-input" 
id="select-member-332051584-60613"><label for="select-member-332051584-60613"><span class="a11y-text">Select 
member</span></label></div><div class="member-block"><div class="member-entity "><a class="entity-container 
entity-link js-member-entity-link" href="https://www.linkedin.com/in/eng-stephen-lubwama-59425393"><figure 
class="entity-figure"><img src="https://media.licdn.com/mpr/mpr/shrink_100_100/gcrc/dms/image/C4E03AQHVeKwtp5FFPA
/profile-displayphoto-shrink_100_100/0?e=1531958400&amp;v=beta&amp;t=iUuMljOKBpQDwIz4_7yRQnZAsGz6B_7Xyu-2L0a0Gjc" 
class="entity-image member-image " alt="Eng. Stephen Lubwama" width="100" height="100"></figure><div 
class="entity-info"><p class="entity-name"><span class="js-hovercard entity-name-text" 
data-membership-id="ADcAABPKtIABdZw1d466KlylzqyyGsp8sXEAzcA-60613" id="hovercard-622">Eng. Stephen 
Lubwama</span></p><p class="entity-headline">Joint CEO at Infotek320 &amp; Elitejobs Uganda</p><p 
class="entity-subheading"></p></div></a></div></div><ul class="member-actions-list"><li class="member-action-item"><a 
class="js-message-member message-member" title="Message" 
href="https://www.linkedin.com/messaging/compose?connId=332051584&amp;groupId=60613"><li-icon type="envelope-icon" 
size="small" class="icon " style="" aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 
24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="small-icon" 
style="fill-opacity: 1"> <g> <path d="M14,3H2C1.4,3,1,3.4,1,4v8c0,0.6,0.4,1,1,1h12c0.6,0,1-0.4,1-1V4C15,3.4,14.6,3,
14,3zM12.4,5L8,8.3L3.6,5H12.4zM3,11V6.7l3.8,2.9C7.2,9.9,7.6,10,8,10c0.4,0,0.8-0.1,1.2-0.4L13,6.8V11H3z"></path> </g> 
</g></svg></svg></li-icon><span class="action-text">Message</span></a></li></ul></li></ul><div class="pagination"><a 
class="pagination-link prev"><li-icon type="chevron-left-icon" size="large" class="icon " style="" 
aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" 
y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="large-icon" style="fill: currentColor"> <g> <path 
d="M16,3l-6.2,9L16,21L14.6,22l-6.3-9.1C8.1,12.6,8,12.3,8,12c0-0.3,0.1-0.6,0.3-0.9l6.3-9.1L16,3z"></path> </g> 
</g></svg></svg></li-icon><span class="link-text">Prev</span></a><a class="pagination-link next"><span 
class="link-text">Next</span><li-icon type="chevron-right-icon" size="large" class="icon " style="" 
aria-hidden="true"><svg style="height: 100%; width: 100%;"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" 
y="0" preserveAspectRatio="xMinYMin meet" class="icon"><g class="large-icon" style="fill: currentColor"> <g> <path 
d="M9.4,2l6.3,9.1C15.9,11.4,16,11.7,16,12c0,0.3-0.1,0.6-0.3,0.9L9.4,22L8,21L14.2,12L8,3L9.4,2z"></path> </g> 
</g></svg></svg></li-icon></a></div></div> 

"""

users = []
ul = []


def search_bing(name):
    n = name.replace(" ", "+")
    driver.get("https://duckduckgo.com/?q=linkedin+" + n + "&t=hb&ia=web")
    time.sleep(3)
    s = BeautifulSoup(driver.page_source, 'lxml')
    fr = s.find("div", class_="result__body links_main links_deep")

    for a in fr.find_all('a'):
        try:
            if 'linkedin.com/in' in a['href']:
                print('found linkedin url', a['href'])
                if a['href'] in ul:
                    print('skipping dup')
                else:
                    ul.append(a['href'])
                    c.writerow([name, a['href']])
                    break
        except Exception as e:
            print(e, '..continue')


soup = BeautifulSoup(your_groups_code, 'lxml')
for a in soup.find_all('img'):
    name = a['alt']
    if name in users:
        print('skipping dup')
    else:
        users.append(name)

if len(users) > 1:
    print('LIST -->', users)
    for i in users:
        print("Scraping", i)
        search_bing(i)
else:
    print('Try again!')
